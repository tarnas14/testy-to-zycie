﻿using System;

namespace TestyToZycie
{
    public class CannotDivideByZeroException : ArgumentOutOfRangeException
    {
    }
}