﻿# Testy to życie

UWAGA UWAGA
Cwiczenie ma na celu jedynie przybliżyć strukturę testów, użycie asercji nunita, podstaw r# i odpalania testów w vs :)
NIE próbujemy tu napisać kalkulatora :3

1. Przetestować metodę `Add`
    - Upewnić się że działa dla różnych przypadków (dodatnie, ujemne...)

2. Przetestować metodę `Divide`
    - Upewnić się że działa dla różnych przypadków (dodatnie, ujemne...)
    - Upewnić się że nie da się dzielić przez zero
    
3. Zmienić metodę `Add` tak żeby przyjmowała kolekcję składników do dodania

4. Przetestować metodę `LookAtMyAbs`

// holy crap piszemy kod

5. Sprawić żeby kalkulator rzucał `YouShouldNotCheatOnYourExamsException` w każdej metodzie jeśli używa się go między godziną 8:00AM i 8:45AM